FROM maven:3.8.3-openjdk-17 as MAVEN_BUILD
MAINTAINER gjamot
COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn clean install && mvn package -B

FROM openjdk:17-jdk-slim-buster
WORKDIR /app
COPY --from=MAVEN_BUILD /build/target/high-school-note-app-*.jar /app/high-school-note-app.jar
ENTRYPOINT ["java", "-jar", "high-school-note-app.jar"]

EXPOSE 8091
