package com.gjamot.serverparent.controller;

import com.gjamot.serverparent.model.LevelMainTeacher;
import com.gjamot.serverparent.repository.LevelMainTeacherRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping({"/api"})
public class LevelMainTeacherController {
    @Autowired
    LevelMainTeacherRepository levelMainTeacherRepository;


    @Operation(
            summary = "Get classes list with main teacher and associated students"
    )
    @ApiResponses({@ApiResponse(
            responseCode = "200",
            description = "Found classes list",
            content = {@Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            implementation = LevelMainTeacher.class
                    )
            )}
    )})
    @GetMapping({"/classes"})
    public ResponseEntity<Set<LevelMainTeacher>> getAllClassesWithMainTeacherAndAssociatedStudents() {
        Set<LevelMainTeacher> classes = new HashSet<>(this.levelMainTeacherRepository.findAll());
        return classes.isEmpty() ? new ResponseEntity<>(HttpStatus.NO_CONTENT) : new ResponseEntity<>(classes, HttpStatus.OK);
    }
}

