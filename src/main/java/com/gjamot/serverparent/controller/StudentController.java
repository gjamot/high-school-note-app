//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.gjamot.serverparent.controller;

import com.gjamot.serverparent.exception.ResourceNotFoundException;
import com.gjamot.serverparent.model.Student;
import com.gjamot.serverparent.model.StudentCourseInfo;
import com.gjamot.serverparent.repository.StudentCourseInfoRepository;
import com.gjamot.serverparent.repository.StudentRepositoryId;
import com.gjamot.serverparent.repository.StudentRepositoryLevel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

@RestController
@RequestMapping({"/api"})
public class StudentController {
    @Autowired
    StudentRepositoryId studentRepositoryId;
    @Autowired
    StudentRepositoryLevel studentRepositoryLevel;
    @Autowired
    StudentCourseInfoRepository studentCourseInfoRepository;

    @Operation(
            summary = "Get student list ordered by lastname and firstname"
    )
    @ApiResponses({@ApiResponse(
            responseCode = "200",
            description = "Found student list",
            content = {@Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            implementation = Student.class
                    )
            )}
    )})
    @GetMapping({"/students"})
    public ResponseEntity<TreeSet<Student>> getAllStudents() {
        Comparator<Student> compareByLastName = Comparator.comparing(Student::getLastName);
        Comparator<Student> compareByFirstName = Comparator.comparing(Student::getFirstName);
        Comparator<Student> compareByFullName = compareByLastName.thenComparing(compareByFirstName);
        TreeSet<Student> students = new TreeSet<>(compareByFullName);
        students.addAll(this.studentRepositoryId.findAll());
        return students.isEmpty() ? new ResponseEntity<>(HttpStatus.NO_CONTENT) : new ResponseEntity<>(students, HttpStatus.OK);
    }

    @Operation(
            summary = "Get student list by class given in parameter"
    )
    @ApiResponses({@ApiResponse(
            responseCode = "200",
            description = "Found student list by class",
            content = {@Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            implementation = Student.class
                    )
            )}
    )})
    @GetMapping({"/students/level/{level}"})
    public ResponseEntity<TreeSet<Student>> getAllStudentsByLevel(@PathVariable("level") String level) {
        Comparator<Student> compareByLastName = Comparator.comparing(Student::getLastName);
        Comparator<Student> compareByFirstName = Comparator.comparing(Student::getFirstName);
        Comparator<Student> compareByFullName = compareByLastName.thenComparing(compareByFirstName);
        TreeSet<Student> students = new TreeSet<>(compareByFullName);
        students.addAll(this.studentRepositoryLevel.findAllStudentsByLevel(level));
        return students.isEmpty() ? new ResponseEntity<>(HttpStatus.NO_CONTENT) : new ResponseEntity<>(students, HttpStatus.OK);
    }

    @Operation(
            summary = "Get a student by his id"
    )
    @ApiResponses({@ApiResponse(
            responseCode = "200",
            description = "Found the student",
            content = {@Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            implementation = Student.class
                    )
            )}
    ), @ApiResponse(
            responseCode = "400",
            description = "Invalid id supplied",
            content = {@Content}
    ), @ApiResponse(
            responseCode = "404",
            description = "Student not found",
            content = {@Content}
    )})
    @GetMapping({"/students/{id}"})
    public ResponseEntity<Student> getStudentById(@PathVariable("id") long id) {
        Student student = this.studentRepositoryId.findById(id).orElseThrow(() -> new ResourceNotFoundException("Student not found with id = " + id));
        return new ResponseEntity<>(student, HttpStatus.OK);
    }

    @Operation(
            summary = "Get student course info list from a student id"
    )
    @ApiResponses({@ApiResponse(
            responseCode = "200",
            description = "Found student course info list",
            content = {@Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            implementation = StudentCourseInfo.class
                    )
            )}
    )})
    @GetMapping({"/students/{id}/courses/info"})
    public ResponseEntity<Set<StudentCourseInfo>> getStudentCourseInfosByStudentId(@PathVariable("id") long id) {
        Set<StudentCourseInfo> studentCourseInfos = new HashSet<>(this.studentCourseInfoRepository.findStudentCourseInfosByStudentId(id));
        return studentCourseInfos.isEmpty() ? new ResponseEntity<>(HttpStatus.NO_CONTENT) : new ResponseEntity<>(studentCourseInfos, HttpStatus.OK);
    }


    @Operation(
            summary = "Get student course info list with rating from student id"
    )
    @ApiResponses({@ApiResponse(
            responseCode = "200",
            description = "Found student course info list",
            content = {@Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            implementation = StudentCourseInfo.class
                    )
            )}
    )})
    @GetMapping({"/students/{id}/ratings"})
    public ResponseEntity<Set<StudentCourseInfo>> getStudentsRatingByStudentIdAndGroupByCourseName(@PathVariable("id") long id) {
        Set<StudentCourseInfo> studentCourseInfos = new HashSet<>(this.studentCourseInfoRepository.findStudentCourseInfosByStudentIdGroupAndOrderByCourseName(id));
        return studentCourseInfos.isEmpty() ? new ResponseEntity<>(HttpStatus.NO_CONTENT) : new ResponseEntity<>(studentCourseInfos, HttpStatus.OK);
    }

    @Operation(
            summary = "Creates a new student"
    )
    @ApiResponses({@ApiResponse(
            responseCode = "200",
            description = "Student created with success",
            content = {@Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            implementation = Student.class
                    )
            )}
    )})
    @PostMapping({"/students"})
    public ResponseEntity<Student> createStudent(@Valid @RequestBody Student student) {
        Student _student = this.studentRepositoryId.save(student);
        return new ResponseEntity<>(_student, HttpStatus.CREATED);
    }

    @Operation(
            summary = "Updates a new student from student id"
    )
    @ApiResponses({@ApiResponse(
            responseCode = "200",
            description = "Student udpated with success",
            content = {@Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            implementation = Student.class
                    )
            )}
    )})
    @PutMapping({"/students/{id}"})
    public ResponseEntity<Student> updateStudent(@PathVariable("id") long id, @RequestBody Student student) {
        Student _student = this.studentRepositoryId.findById(id).orElseThrow(() -> new ResourceNotFoundException("Student not found with id = " + id));
        _student.setFirstName(student.getFirstName());
        _student.setLastName(student.getLastName());
        _student.setSex(student.getSex());
        _student.setMainTeacher(student.getMainTeacher());
        _student.setLevel(student.getLevel());
        return new ResponseEntity<>(this.studentRepositoryId.save(_student), HttpStatus.OK);
    }

}
