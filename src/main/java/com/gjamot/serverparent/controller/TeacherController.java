package com.gjamot.serverparent.controller;

import com.gjamot.serverparent.exception.ResourceNotFoundException;
import com.gjamot.serverparent.model.Teacher;
import com.gjamot.serverparent.repository.TeacherRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.TreeSet;

@RestController
@RequestMapping({"/api"})
public class TeacherController {
    @Autowired
    TeacherRepository teacherRepository;

    @Operation(
            summary = "Get teacher list ordered by lastname and firstname"
    )
    @ApiResponses({@ApiResponse(
            responseCode = "200",
            description = "Found teacher list",
            content = {@Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            implementation = Teacher.class
                    )
            )}
    )})
    @GetMapping({"/teachers"})
    public ResponseEntity<TreeSet<Teacher>> getAllTeachers() {
        Comparator<Teacher> compareByLastName = Comparator.comparing(Teacher::getLastName);
        Comparator<Teacher> compareByFirstName = Comparator.comparing(Teacher::getFirstName);
        Comparator<Teacher> compareByFullName = compareByLastName.thenComparing(compareByFirstName);
        TreeSet<Teacher> teachers = new TreeSet<>(compareByFullName);
        teachers.addAll(this.teacherRepository.findAll());
        return teachers.isEmpty() ? new ResponseEntity<>(HttpStatus.NO_CONTENT) : new ResponseEntity<>(teachers, HttpStatus.OK);
    }

    @Operation(
            summary = "Get a teacher by his id"
    )
    @ApiResponses({@ApiResponse(
            responseCode = "200",
            description = "Found the teacher",
            content = {@Content(
                    mediaType = "application/json",
                    schema = @Schema(
                            implementation = Teacher.class
                    )
            )}
    ), @ApiResponse(
            responseCode = "400",
            description = "Invalid id supplied",
            content = {@Content}
    ), @ApiResponse(
            responseCode = "404",
            description = "Teacher not found",
            content = {@Content}
    )})
    @GetMapping({"/teachers/{id}"})
    public ResponseEntity<Teacher> getTeacherById(@PathVariable("id") long id) {
        Teacher teacher = this.teacherRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Teacher not found with id = " + id));
        return new ResponseEntity<>(teacher, HttpStatus.OK);
    }
}