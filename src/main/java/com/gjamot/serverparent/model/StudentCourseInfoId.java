package com.gjamot.serverparent.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class StudentCourseInfoId implements Serializable {

    @Column(name = "main_teacher_id")
    long mainTeacherId;

    @Column(name = "level_name")
    String levelName;

    @Column(name = "course_name")
    String courseName;

    @Column(name = "teacher_id")
    long teacherId;

    @Column(name = "student_id")
    long studentId;

}
