package com.gjamot.serverparent.model;

public enum Sex {
    M,
    F,
    NG
}
