package com.gjamot.serverparent.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public
class LevelMainTeacherId implements Serializable {

    @Column(name = "main_teacher_id")
    long mainTeacherId;

    @Column(name = "level_name")
    String levelName;

}
