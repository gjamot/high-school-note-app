package com.gjamot.serverparent.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
@NoArgsConstructor
@Getter
@Setter
@Entity
public class StudentCourseInfo {

    @EmbeddedId
    StudentCourseInfoId id;

    @ManyToOne
    @MapsId("mainTeacherId")
    @JoinColumn(name = "main_teacher_id")
    Teacher mainTeacher;

    @ManyToOne
    @MapsId("levelName")
    @JoinColumn(name = "level_name")
    Level level;

    @ManyToOne
    @MapsId("courseName")
    @JoinColumn(name = "course_name")
    Course course;

    @ManyToOne
    @MapsId("teacherId")
    @JoinColumn(name = "teacher_id")
    Teacher teacher;

    @ManyToOne
    @MapsId("studentId")
    @JoinColumn(name = "student_id")
    Student student;

    @Column(name = "rating")
    private Double rating;

    @Column(name = "assessment")
    private String assessment;

}
