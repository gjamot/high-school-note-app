package com.gjamot.serverparent.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Set;
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "teacher")
public class Teacher implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable=false, name = "first_name")
    @NotEmpty
    private String firstName;

    @Column(nullable=false, name = "last_name")
    @NotEmpty
    private String lastName;

    @Column(nullable=false)
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @ManyToMany(mappedBy = "mainTeachers")
    Set<Level> levels;

    @ManyToMany(mappedBy = "teachers")
    Set<Course> courses;

}
