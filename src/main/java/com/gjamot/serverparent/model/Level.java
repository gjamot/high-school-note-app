package com.gjamot.serverparent.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Set;
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "level")
public class Level  implements Serializable {
    @Id
    @Column(nullable=false)
    @NotEmpty
    private String name;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "class",
            joinColumns = @JoinColumn(name = "level_name"),
            inverseJoinColumns = @JoinColumn(name = "main_teacher_id"))
    private Set<Teacher> mainTeachers;

}

