package com.gjamot.serverparent.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.io.Serializable;
@NoArgsConstructor
@Data
@Entity
@Table(name = "class")
public class LevelMainTeacher  implements Serializable {
    @EmbeddedId
    LevelMainTeacherId id;

    @ManyToOne
    @MapsId("mainTeacherId")
    @JoinColumn(name = "main_teacher_id")
    Teacher mainTeacher;

    @ManyToOne
    @MapsId("levelName")
    @JoinColumn(name = "level_name")
    Level level;

}
