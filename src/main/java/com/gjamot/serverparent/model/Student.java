package com.gjamot.serverparent.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "student")
public class Student implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable=false, name = "first_name")
    @NotEmpty
    private String firstName;

    @Column(nullable=false, name = "last_name")
    @NotEmpty
    private String lastName;

    @Column(nullable=false)
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="main_teacher_id", referencedColumnName="id")
    private Teacher mainTeacher;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="level_name", referencedColumnName="name")
    private Level level;
}
