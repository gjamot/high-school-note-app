package com.gjamot.serverparent.repository;

import com.gjamot.serverparent.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepositoryId extends JpaRepository<Student, Long> {
}
