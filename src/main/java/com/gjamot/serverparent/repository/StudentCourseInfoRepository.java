package com.gjamot.serverparent.repository;

import com.gjamot.serverparent.model.StudentCourseInfo;
import com.gjamot.serverparent.model.StudentCourseInfoId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StudentCourseInfoRepository extends CrudRepository<StudentCourseInfo, StudentCourseInfoId> {
    @Query(value = "SELECT sci.* FROM student_course_info sci WHERE sci.student_id= ?1", nativeQuery = true)
    List<StudentCourseInfo> findStudentCourseInfosByStudentId(Long studentId);

    @Query(value = "SELECT sci.* FROM student_course_info sci WHERE sci.student_id= ?1 GROUP BY sci.course_name ORDER BY sci.course_name", nativeQuery = true)
    List<StudentCourseInfo> findStudentCourseInfosByStudentIdGroupAndOrderByCourseName(Long studentId);
}