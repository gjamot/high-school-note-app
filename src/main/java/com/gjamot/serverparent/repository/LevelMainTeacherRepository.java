package com.gjamot.serverparent.repository;

import com.gjamot.serverparent.model.LevelMainTeacher;
import com.gjamot.serverparent.model.LevelMainTeacherId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LevelMainTeacherRepository extends CrudRepository<LevelMainTeacher, LevelMainTeacherId> {
    @Query(value = "SELECT lmt FROM LevelMainTeacher AS lmt")
    List<LevelMainTeacher> findAll();
}