package com.gjamot.serverparent.repository;

import com.gjamot.serverparent.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepositoryLevel extends JpaRepository<Student, String> {
    @Query(value = "SELECT s.* FROM student s WHERE s.level_name= ?1", nativeQuery = true)
    List<Student> findAllStudentsByLevel(String level);
}
