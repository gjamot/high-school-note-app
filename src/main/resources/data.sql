ALTER TABLE student_course_info
ADD CONSTRAINT MAX_RATING_CHECK CHECK (rating BETWEEN 0 AND 20);

INSERT INTO level (name) VALUES
    ('6e'),
    ('5e'),
    ('4e'),
    ('3e');

INSERT INTO teacher (id, first_name, last_name, sex) VALUES
    (1, 'paul', 'dupont', 'M'),
    (2, 'paula', 'robert', 'F'),
    (3, 'bob', 'lambert', 'M'),
    (4, 'alice', 'tommy', 'F'),
    (5, 'yoko', 'yuki', 'F');


INSERT INTO class (main_teacher_id, level_name) VALUES
    (1, '6e'),
    (5, '5e'),
    (3, '4e'),
    (4, '3e');

INSERT INTO student (id, first_name, last_name, sex, main_teacher_id, level_name) VALUES
    (1, 'john', 'doe_6e', 'M', 1, '6e'),
    (2, 'jane', 'doe_6e', 'F', 1, '6e'),
    (3, 'john', 'doe_5e', 'M', 5, '5e'),
    (4, 'jane', 'doe_5e', 'F', 5, '5e'),
    (5, 'john', 'doe_4e', 'M', 3, '4e'),
    (6, 'jane', 'doe_4e', 'F', 3, '4e'),
    (7, 'john', 'doe_3e', 'M', 4, '3e'),
    (8, 'jane', 'doe_3e', 'F', 4, '3e');


INSERT INTO course (name) VALUES
    ('Mathématiques'),
    ('Italien'),
    ('Physique-Chimie'),
    ('SVT'),
    ('Anglais');


INSERT INTO teacher_course (teacher_id, course_name) VALUES
    (1, 'Italien'),
    (5, 'Mathématiques'),
    (3, 'Physique-Chimie'),
    (4, 'SVT'),
    (2, 'Anglais');


INSERT INTO student_course_info(main_teacher_id, level_name, course_name, teacher_id, student_id, rating, assessment) VALUES
    (1, '6e', 'Italien', 1, 1, 12.5, 'Bon travail'),
    (1, '6e', 'Italien', 1, 2, 10.5, 'Passable'),
    (5, '5e', 'Mathématiques', 5, 3, 18.5, 'Excellent !'),
    (5, '5e', 'Mathématiques', 5, 4, 12.5, 'Bon travail'),
    (3, '4e', 'Physique-Chimie', 3, 5, 12.5, 'Bon travail'),
    (3, '4e', 'Physique-Chimie', 3, 6, 12.5, 'Bon travail'),
    (4, '3e', 'SVT', 4, 7, 8.5, 'Des progrès à faire'),
    (4, '3e', 'SVT', 4, 8, 2, 'Remise en question nécessaire...');
