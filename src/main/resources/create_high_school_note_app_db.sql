/* ---------------------------------------------------------------------- */
/* Tables                                                                 */
/* ---------------------------------------------------------------------- */

/* ---------------------------------------------------------------------- */
/* Add table "student"                                                    */
/* ---------------------------------------------------------------------- */

CREATE TABLE student (
    id INTEGER   NOT NULL,
    first_name CHARACTER VARYING(40)   NOT NULL,
    last_name CHARACTER VARYING(40)   NOT NULL,
    sex CHARACTER VARYING(40)   NOT NULL,
    main_teacher_id INTEGER   NOT NULL,
    level_name CHARACTER VARYING(40)   NOT NULL,
    PRIMARY KEY (id, main_teacher_id, level_name)
);

CREATE INDEX IDX_student_%id ON student (id);

/* ---------------------------------------------------------------------- */
/* Add table "teacher"                                                    */
/* ---------------------------------------------------------------------- */

CREATE TABLE teacher (
    id INTEGER   NOT NULL,
    first_name CHARACTER VARYING(40)   NOT NULL,
    last_name CHARACTER VARYING(40)   NOT NULL,
    sex CHARACTER VARYING(2)   NOT NULL,
    CONSTRAINT PK_teacher PRIMARY KEY (id)
);

CREATE INDEX IDX_teacher_id ON teacher (id);

/* ---------------------------------------------------------------------- */
/* Add table "level"                                                      */
/* ---------------------------------------------------------------------- */

CREATE TABLE level (
    name CHARACTER VARYING(40)   NOT NULL,
    CONSTRAINT PK_level PRIMARY KEY (name)
);

CREATE INDEX IDX_level_name ON level (name);

/* ---------------------------------------------------------------------- */
/* Add table "course"                                                     */
/* ---------------------------------------------------------------------- */

CREATE TABLE course (
    name CHARACTER VARYING(40)   NOT NULL,
    CONSTRAINT PK_course PRIMARY KEY (name)
);

CREATE INDEX IDX_course_name ON course (name);

/* ---------------------------------------------------------------------- */
/* Add table "student_course_info"                                        */
/* ---------------------------------------------------------------------- */

CREATE TABLE student_course_info (
    main_teacher_id INTEGER   NOT NULL,
    level_name CHARACTER VARYING(40)   NOT NULL,
    course_name CHARACTER VARYING(40)   NOT NULL,
    teacher_id INTEGER   NOT NULL,
    student_id INTEGER   NOT NULL,
    rating DOUBLE PRECISION ,
    assessment CHARACTER VARYING(255) ,
    PRIMARY KEY (main_teacher_id, level_name, course_name, teacher_id, student_id)
);

CREATE INDEX IDX_student_course_info_id ON student_course_info (teacher_id,student_id,course_name,level_name);

/* ---------------------------------------------------------------------- */
/* Add table "teacher_course"                                             */
/* ---------------------------------------------------------------------- */

CREATE TABLE teacher_course (
    teacher_id INTEGER   NOT NULL,
    course_name CHARACTER VARYING(40)   NOT NULL,
    CONSTRAINT PK_teacher_course PRIMARY KEY (teacher_id, course_name)
);

CREATE INDEX IDX_teacher_course_id ON teacher_course (course_name,teacher_id);

/* ---------------------------------------------------------------------- */
/* Add table "class"                                                      */
/* ---------------------------------------------------------------------- */

CREATE TABLE class (
    main_teacher_id INTEGER   NOT NULL,
    level_name CHARACTER VARYING(40)   NOT NULL,
    CONSTRAINT PK_class PRIMARY KEY (main_teacher_id, level_name)
);

CREATE INDEX IDX_class_id ON class (level_name,main_teacher_id);

/* ---------------------------------------------------------------------- */
/* Foreign key constraints                                                */
/* ---------------------------------------------------------------------- */

ALTER TABLE student ADD CONSTRAINT class_student 
    FOREIGN KEY (main_teacher_id, level_name) REFERENCES class (main_teacher_id,level_name);

ALTER TABLE student_course_info ADD CONSTRAINT teacher_student_course_info 
    FOREIGN KEY (teacher_id) REFERENCES teacher (id);

ALTER TABLE student_course_info ADD CONSTRAINT student_student_course_info 
    FOREIGN KEY (student_id, main_teacher_id, level_name) REFERENCES student (id,main_teacher_id,level_name);

ALTER TABLE student_course_info ADD CONSTRAINT course_student_course_info 
    FOREIGN KEY (course_name) REFERENCES course (name);

ALTER TABLE teacher_course ADD CONSTRAINT course_teacher_course 
    FOREIGN KEY (course_name) REFERENCES course (name);

ALTER TABLE teacher_course ADD CONSTRAINT teacher_teacher_course 
    FOREIGN KEY (teacher_id) REFERENCES teacher (id);

ALTER TABLE class ADD CONSTRAINT level_class 
    FOREIGN KEY (level_name) REFERENCES level (name);

ALTER TABLE class ADD CONSTRAINT teacher_class 
    FOREIGN KEY (main_teacher_id) REFERENCES teacher (id);
