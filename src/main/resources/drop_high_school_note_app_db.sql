/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */

ALTER TABLE student DROP CONSTRAINT class_student;

ALTER TABLE student_course_info DROP CONSTRAINT teacher_student_course_info;

ALTER TABLE student_course_info DROP CONSTRAINT student_student_course_info;

ALTER TABLE student_course_info DROP CONSTRAINT course_student_course_info;

ALTER TABLE teacher_course DROP CONSTRAINT course_teacher_course;

ALTER TABLE teacher_course DROP CONSTRAINT teacher_teacher_course;

ALTER TABLE class DROP CONSTRAINT level_class;

ALTER TABLE class DROP CONSTRAINT teacher_class;

/* ---------------------------------------------------------------------- */
/* Drop table "student_course_info"                                       */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

/* Drop table */

DROP TABLE student_course_info;

/* ---------------------------------------------------------------------- */
/* Drop table "student"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

/* Drop table */

DROP TABLE student;

/* ---------------------------------------------------------------------- */
/* Drop table "class"                                                     */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE class DROP CONSTRAINT PK_class;

/* Drop table */

DROP TABLE class;

/* ---------------------------------------------------------------------- */
/* Drop table "teacher_course"                                            */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE teacher_course DROP CONSTRAINT PK_teacher_course;

/* Drop table */

DROP TABLE teacher_course;

/* ---------------------------------------------------------------------- */
/* Drop table "course"                                                    */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE course DROP CONSTRAINT PK_course;

/* Drop table */

DROP TABLE course;

/* ---------------------------------------------------------------------- */
/* Drop table "level"                                                     */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE level DROP CONSTRAINT PK_level;

/* Drop table */

DROP TABLE level;

/* ---------------------------------------------------------------------- */
/* Drop table "teacher"                                                   */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE teacher DROP CONSTRAINT PK_teacher;

/* Drop table */

DROP TABLE teacher;
