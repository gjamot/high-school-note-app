# high-school-note-app
## Getting started
mvn clean install <br/>
mvn spring-boot:run <br/>
ou <br/>
docker build -t high-school-note-app . <br/>

## h2 database
http://localhost:8091/h2-console <br/>
JDBC URL: jdbc:h2:mem:high-scool-note-app-db <br/>
User Name: sa <br/>
Password: password <br/>

## swagger url
http://localhost:8091/swagger-ui/index.html#/ <br/>
